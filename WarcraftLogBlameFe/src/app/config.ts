export let BASE_URL = window.location.origin + '/api';
export let FRONT_URL = window.location.origin;
if (BASE_URL.indexOf('localhost') != -1) {
  BASE_URL = 'http://localhost:5000';
  FRONT_URL = 'http://localhost:4200';
}


// export const BASE_URL = "http://localhost:5000";

