import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportService} from '../report.service';
import { Event, Button } from '../event';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fight',
  templateUrl: './fight.component.html',
  styleUrls: ['./fight.component.css']
})
export class FightComponent implements OnInit, OnDestroy {
  reportTag: string;
  savedFight: any;
  fightId: string;
  showEvents: Event[];
  allEvents: Event[];
  filtredEvents: Event[];
  buttonsIsDissabled = true;
  displayEvents = {
    player_death: true,
    healer_save: true,
    boss_event: true,
    boss_health: true,
    cast_event: true,
    boss_mana: true,
    decurse_event: true,
  };
  eventsIcons = {
    player_death: 'sentiment_very_dissatisfied',
    healer_save: 'local_hospital',
    boss_event: 'whatshot',
    boss_health: 'favorite_border',
    cast_event: 'double_arrow',
    boss_mana: 'local_bar',
    decurse_event: 'spa',
  };
  existEvents = {
    player_death: false,
    healer_save: false,
    boss_event: false,
    boss_health: false,
    cast_event: false,
    boss_mana: false,
    decurse_event: false,
  };
  loading: number;
  private subscription: Subscription = new Subscription();
  filterButtons: Button[];


  constructor(
    private route: ActivatedRoute,
    private reportService: ReportService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.loading = 3;
    this.reportTag = this.route.snapshot.paramMap.get('report_id');
    this.fightId = this.route.snapshot.paramMap.get('fight_id');
    this.showEvents = [];
    this.subscription.add(this.reportService.obsFight.subscribe(data => {
      this.savedFight = data;
      if (this.savedFight == null) {
        this.reportService.sendReport(this.reportTag).subscribe(response => {
          this.reportService.saveReport(response) ;
          if (response.report.fights) {
            for (const fight of response.report.fights) {
              if (fight.id === Number(this.fightId)) {
                this.reportService.saveFight(fight);
              }
            }
          }
        });
      }
    }));

    this.reportService.healSavesReport(this.reportTag, this.fightId).subscribe(response => {
      this.loading -= 1;
      this.process_new_results(response);
    });
    this.reportService.deathReport(this.reportTag, this.fightId).subscribe(response => {
      this.loading -= 1;
      this.process_new_results(response);
    });
    this.reportService.bossReport(this.reportTag, this.fightId).subscribe(response => {
      this.loading -= 1;
      this.process_new_results(response);
    });

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  process_new_results(response) {
    const eventsSize = this.showEvents.length;
    let tmpEvents: Event[] = [];
    for (const event of response.events) {
      tmpEvents.push(event as Event);
    }
    tmpEvents = tmpEvents.concat(this.showEvents);
    if (tmpEvents.length > 0) {
      tmpEvents.sort((a, b) => (a.timestamp > b.timestamp) ? 1 : -1);
    }
    if (this.showEvents.length == eventsSize) {
      this.showEvents = tmpEvents;
      this.allEvents = tmpEvents;
      this.eventChecker();
      this.buttonsCreation();
    } else {
      this.process_new_results(response);
    }
    if (this.loading === 0) {
      this.buttonsIsDissabled = false;
    }
  }

  timePresent(miliseconds): string {
    let time;
    time = this.reportService.convertMiliseconds(miliseconds);
    return time;
  }

  eventSorter(): void {
    if (this.showEvents.length > 0) {
      this.showEvents.sort((a, b) => (a.timestamp > b.timestamp) ? 1 : -1);
    }
  }

  onFilterClick(event): void {
    this.displayEvents[event.value] = !this.displayEvents[event.value];
    this.eventFilter();
  }


  eventFilter(): void {
    this.buttonsIsDissabled = true;
    this.filtredEvents = [];
    for (const event of this.allEvents) {
      if (this.displayEvents[event.tag]) {
        this.filtredEvents.push(event as Event); }
    }
    this.showEvents = this.filtredEvents;
    this.buttonsIsDissabled = false;
  }

  eventChecker(): void { for (const event of this.allEvents) {
      this.existEvents[event.tag] = true;  }
  }
  buttonsCreation(): void {
    this.filterButtons = [];
    for (const element in this.displayEvents) {
      let tag = element;
      let show = this.existEvents[element];
      let matIcon = this.eventsIcons[element];
      if (show) {
        this.filterButtons.push({tag, show, matIcon});
      }
    }
  }

  get_message_color(colorCode): string{
    switch (colorCode) {
      case 0: return 'black';   // common
      case 1: return 'brown';   // death
      case 2: return 'green';   // heal_saves
      case 3: return '#5E0FC4';    // boss_mechanicks
      case 4: return 'purple';  // boss_hp
      case 5: return 'orange';  // mistakes
      case 6: return 'red';     // big mistakes
      case 11: return '#209B78';  // monk
      case 12: return '#FF7D0A';  // druid
      case 13: return '#C79C6E';  // warrior
      case 14: return '#D9D349';  // rogue
      case 15: return '#C41F3B';  // death_king
      case 16: return '#0326EF';  // shaman
      case 17: return '#9530BD';   // demon_hunter
      case 18: return '#ABD44E';  // hunter
      case 19: return '#9482BC';  // warlock
      case 20: return 'grey';    // priest
      case 21: return '#F58CAA';  // paladin
      case 22: return '#47CCF0';     // mage
      default: return  'black';
    }
  }

  getState(state): string {
    if (state) {
      return 'Kill';
    }
    return 'Wipe';
  }

  return_back(): void {
    this.router.navigate([`reports`, this.reportTag]);
  }

  go_to_wl(): void {
    const url = 'https://www.warcraftlogs.com/reports/' + this.reportTag + '#fight=' +  this.fightId;
    window.open(url, '_blank');
  }

}
