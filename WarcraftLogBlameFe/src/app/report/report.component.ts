import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportService } from '../report.service';
import { Fight } from '../report';
import { Subscription } from 'rxjs';
import { FRONT_URL } from '../config';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit, OnDestroy {
  fights: Fight[];
  tmpFights: Fight[];
  savedReport: any;
  reportTag: string;
  private subscription: Subscription = new Subscription();
  constructor(
    private route: ActivatedRoute,
    private reportService: ReportService,
    private router: Router,
  ) { }



  ngOnInit() {
    this.fights = [];
    this.tmpFights = [];
    this.subscription.add(this.reportService.obsFightsInReport
      .subscribe(data => {
        this.savedReport = data;
        // console.log('observable', data);
        this.reportTag = this.route.snapshot.paramMap.get('report_id');
        if (this.reportService.fightsInReport.getValue() && this.reportService.fightsInReport.getValue().report_code === this.reportTag) {
          for (const item of this.savedReport.report.fights) {
            if (item.boss !== 0) {
              this.tmpFights.push(item as Fight);
            }
          }
        }
      }));
    this.reportService.sendReport(this.reportTag).subscribe(response => {
      // console.log(response);
      this.reportService.saveReport(response) ;
      if (response.report.fights) {
        for (const item of response.report.fights) {
          if (item.boss !== 0) {
            this.fights.push(item as Fight);
          }
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  timePresent(miliseconds): string {
    let time;
    time = this.reportService.convertMiliseconds(miliseconds);
    return time.slice(0,-3);
  }

  reportClick(fight): void {
    this.router.navigate([`reports`, this.reportTag, fight.id ]);
    this.reportService.saveFight(fight);
  }

  synthesLinkForFight(fight): string {
    return FRONT_URL + '/reports/' + this.reportTag + '/' + fight.id;
  }

  return_back(): void {
    this.router.navigate([`index`]);
  }
}
