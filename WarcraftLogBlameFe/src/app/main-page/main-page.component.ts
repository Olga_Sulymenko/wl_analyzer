import { Component, OnInit } from '@angular/core';
import { ReportService} from '../report.service';
import { FormControl} from '@angular/forms';
import { Fight } from '../report';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  fights: Fight[];
  title = 'WarcraftLogBlameFe';
  InputUrl = new FormControl();
  reportTag: any;
  constructor(
    private reportService: ReportService,
    private router: Router,
  )  { }


  redirectToReport(): void {
    const inputReport = this.InputUrl.value;
    let startPoint = inputReport.indexOf('reports/') + 8;
    let endPoint =  startPoint + 16;
    this.reportTag = inputReport.slice(startPoint, endPoint);
    startPoint = inputReport.indexOf('fight=') + 6;
    if (startPoint !== 5) {
      endPoint = inputReport.indexOf('&');
      if (endPoint === -1 && inputReport.length - startPoint > 2) {
        endPoint = startPoint + 2;
      } else {
        endPoint = startPoint + 1;
      }
      let fightId = inputReport.slice(startPoint, endPoint);
      if (isNaN(Number(fightId))) {
        fightId = fightId.slice(0, -1);
        if (isNaN(Number(fightId)) || (fightId.length === 0)) {
          this.router.navigate([`reports`, this.reportTag]);
        } else { this.router.navigate([`reports`, this.reportTag, fightId]); }
      } else { this.router.navigate([`reports`, this.reportTag, fightId]); }
    } else { this.router.navigate([`reports`, this.reportTag]); }
  }



  reportClick(fight): void {
    this.reportService.healSavesReport(this.reportTag, fight.id).subscribe(response => {
      console.log(response); } );
  }


  ngOnInit() {

  }

}
