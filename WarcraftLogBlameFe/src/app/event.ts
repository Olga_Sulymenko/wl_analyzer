export class Event {
  tag: string;
  importance: number;
  message: [];
  timestamp: number;
}

export class Button {
  tag: string;
  show: boolean;
  matIcon: string;
}
