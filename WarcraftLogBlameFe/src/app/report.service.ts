import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from './config';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  private reportUrl = BASE_URL + '/report';
  public fightsInReport = new BehaviorSubject(null);
  obsFightsInReport = this.fightsInReport.asObservable();
  public fight = new BehaviorSubject(null);
  obsFight = this.fight.asObservable();
  constructor(
    private http: HttpClient,
  ) { }


  sendReport(report): Observable<any> {
    const url = `${this.reportUrl}/${report}`;
    return this.http.get(url)
      .pipe( );
  }


  healSavesReport(report, fightId): Observable <any> {
    const url = `${this.reportUrl}/${report}/fight/${fightId}/healer_saves`;
    return this.http.get(url)
      .pipe();
  }

  deathReport(report, fightId): Observable <any> {
    const url = `${this.reportUrl}/${report}/fight/${fightId}/playres_death`;
    return this.http.get(url)
      .pipe();
  }

  bossReport(report, fightId): Observable <any> {
    const url = `${this.reportUrl}/${report}/fight/${fightId}/boss_event`;
    return this.http.get(url)
      .pipe();
  }

  saveReport(report) {
    this.fightsInReport.next(report);
  }

  saveFight(fight) {
    this.fight.next(fight);
  }

  convertMiliseconds(miliSeconds): string {
    const miliseconds = Number(miliSeconds); // convert to number
    let minutes, seconds, totalMinutes, totalSeconds, milisec;

    totalSeconds = Math.floor(miliseconds / 1000);
    totalMinutes = Math.floor(totalSeconds / 60);

    seconds = totalSeconds % 60;
    minutes = totalMinutes % 60;
    milisec = miliseconds % 1000;
    if ( Number(seconds) < 10) {
      seconds = '0' + seconds;
    }
    if ( Number(milisec) < 100) {
      if ( Number(milisec) < 10) {
        milisec = '00' + milisec;
      } else {
        milisec = '0' + milisec;
      }
    }
    milisec = String(milisec).slice(0, -1);
    return minutes + ':' + seconds + ':' + milisec;
  }
}
