export class Fight {
  boss : number;
  name: string;
  id: number;
  start_time: number;
  end_time: number;
  bossPercentage: number;
  kill: boolean;
  lastPhaseForPercentageDisplay: number;
}
