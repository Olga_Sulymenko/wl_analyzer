import os
from dotenv import load_dotenv
load_dotenv()


class Config:
    ENV = 'development'
    DEBUG = True
    CACHE_TYPE = "simple"  # Flask-Caching related configs
    CACHE_DEFAULT_TIMEOUT = 1800
    CACHE_THRESHOLD = 100
    WL_API_V1_TOKEN = os.environ.get("WL_API_V1_TOKEN")
    WL_API_V2_TOKEN = os.environ.get("WL_API_V2_TOKEN")
