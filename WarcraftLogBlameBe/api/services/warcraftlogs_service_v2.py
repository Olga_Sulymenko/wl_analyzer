import requests
import json
import time


class WarCraftLogsServiceV2:
    login_url = "https://www.warcraftlogs.com/oauth/token"
    basic_auth = ""
    access_token = None
    access_token_expiry = None

    api_url_base = "https://www.warcraftlogs.com:443/api/v2/client"

    def __init__(self, basic_auth):
        self.basic_auth = basic_auth

    def refresh_token(self):
        now = int(round(time.time() * 1000))
        if self.access_token is None or now > self.access_token_expiry:
            headers = {'Content-Type': 'application/x-www-form-urlencoded',
                       'Authorization': 'Basic {0}'.format(self.basic_auth)}
            response = requests.post(self.login_url, data="grant_type=client_credentials", headers=headers)
            if response.status_code == 200:
                self.access_token = response.json()['access_token']
                self.access_token_expiry = now + response.json()['expires_in']
            else:
                raise Exception("Can not login to www.warcraftlogs.com using api V2")

    def basic_get(self):
        headers = {
            'Accept': '*/*',
            'Authorization': 'Bearer %s' % self.access_token
        }
        api_url = '{0}'.format(self.api_url_base)
        response = requests.get(api_url, headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_casts_list_v2(self, report_code, abilities,  start, end):
        self.refresh_token()
        headers = {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer %s' % self.access_token
        }
        request = ""
        for ability in abilities:
            if 'name' in ability and 'code' in ability:
                ability_id = ability['code']
                request += 'a%d:reportData{report(code: $rcode){events(startTime:$st, endTime:$et, abilityID:%d, dataType:Casts){data nextPageTimestamp}}}' % (ability_id, ability_id)
        query = 'query($st: Float = %d, $et: Float = %d, $rcode: String = "%s"){ %s }'\
                % (start, end, report_code, request)
        response = requests.post(self.api_url_base, json={'query': query}, headers=headers)

        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            result = []
            for report in body['data']:
                spell_events = body['data'][report]['report']['events']['data']
                spell_result = []
                for new_event in spell_events:
                    if not self.is_duplicate_channel_event(spell_result, new_event):
                        spell_result.append(new_event)
                result.extend(spell_result)
            return result
        else:
            raise Exception

    @staticmethod
    def is_duplicate_channel_event(events, new_event):
        for event in events:
            if event['sourceID'] == new_event['sourceID'] and (abs(event['timestamp'] - new_event['timestamp']) < 8000):
                return True
        return False
