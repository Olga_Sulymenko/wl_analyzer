import boto3
import botocore.exceptions
import json

from model.Guild import Guild


class GuildService:

    BUCKET_NAME = 'wl-analyzer'  # Read this from config?

    def __init__(self):
        self.s3 = boto3.resource('s3')
        self.bucket = self.s3.Bucket('wl-analyzer')
        pass

    def save_guild_definition(self, guild_definition):
        json_dump = json.dumps(guild_definition.json())
        encoded = json_dump.encode('UTF-8')
        try:
            obj = self.s3.Object(self.BUCKET_NAME, "{}/{}/{}/guild.json".format(guild_definition.region.lower(),
                                                                                guild_definition.server.lower(),
                                                                                guild_definition.name.lower()))
            obj.put(Body=encoded)
        except botocore.exceptions.ClientError as e:
            raise Exception("Error saving guild data", e)
        guild = Guild.from_definition(self.get_guild_definition(guild_definition.region.lower(),
                                                                guild_definition.server.lower(),
                                                                guild_definition.name.lower()))
        return guild

    def get_guild_definition(self, region, server, guild_name):
        try:
            obj = self.s3.Object(self.BUCKET_NAME, "{}/{}/{}/guild.json".format(region.lower(),
                                                                          server.lower(), guild_name.lower()))
            body = obj.get()['Body'].read()
            decoded = body.decode('UTF-8')
            json_decoded = json.loads(decoded)
            return json_decoded
        except botocore.exceptions.ClientError as e:
            raise Exception("Error fetching guild data", e)
