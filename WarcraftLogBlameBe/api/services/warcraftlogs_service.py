import requests
import json


class WarCraftLogsService:

    # TODO move this to env variable
    api_token = 'undefined'
    api_url_base = 'https://www.warcraftlogs.com:443/v1'
    headers = {'Content-Type': 'application/json',
               'Authorization': 'Bearer {0}'.format(api_token)}
    color_classes ={
        'Monk': 11,
        'Druid': 12,
        'Warrior': 13,
        'Rogue': 14,
        'DeathKnight': 15,
        'Shaman': 16,
        'DemonHunter': 17,
        'Hunter': 18,
        'Warlock': 19,
        'Priest': 20,
        'Paladin': 21,
        'Mage': 22

    }

    def __init__(self, api_token):
        self.api_token = api_token

    def get_report_fights(self, report_code):
        api_url = '{0}/report/fights/{1}?api_key={2}'.format(self.api_url_base, report_code, self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_death_list(self, report_code, start, end):
        api_url = '{0}/report/events/deaths/{1}?start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_damage_by_enemy_list(self, report_code, start, end):
        api_url = '{0}/report/tables/damage-taken/{1}?hostility=1&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_enemy_casts_list_events(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/events/casts/{report_code}?hostility=1&abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_enemy_casts_list(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/tables/casts/{report_code}?hostility=1&abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_casts_list(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/events/casts/{report_code}?abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    'https://www.warcraftlogs.com:443/v1/report/events/dispels/32X9q6nGPDrgVjCA?start=2163805&end=2258178&api_key=ea71e407bbf895f636ae2797779ff27d'
    'https://www.warcraftlogs.com:443/v1/report/events/dispels/32X9q6nGPDrgVjCA?start=2163805&end=2326572&api_key=c870926e6abb7104e68b56c526fb6811'
    def get_dispell_list(self, report_code, start, end):
        api_url = '{0}/report/events/dispels/{report_code}?start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_casts_list_by_hero(self, report_code, hero_id, ability_id,  start, end):
        api_url = '{0}/report/events/casts/{report_code}?sourceid={sourceid}&abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    sourceid=hero_id,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_boss_health(self, report_code, start, end):
        api_url = '{0}/report/tables/resources/{report_code}?abilityid={abilityid}&hostility=1&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid='1000',
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_boss_mana(self, report_code, start, end):
        api_url = '{0}/report/tables/resources/{report_code}?abilityid={abilityid}&hostility=1&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid='100',
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_debufs_events_on_heroes(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/events/debuffs/{report_code}?abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_player_damage_in_time_window(self, report_code, player_id,  start, end):
        api_url = '{0}/report/tables/damage-done/{report_code}' \
                  '?start={start}&end={end}&by=target&sourceid={player_id}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    player_id=player_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_players_damage_in_time_window(self, report_code, start, end):
        api_url = '{0}/report/tables/damage-done/{report_code}' \
                  '?start={start}&end={end}&by=target&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_buffs_list_on_bosses(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/events/buffs/{report_code}?abilityid={abilityid}&hostility=1&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_debufs_list_on_heroes(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/tables/debuffs/{report_code}?abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_damage_receive_events(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/events/damage-taken/{report_code}?abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            if 'nextPageTimestamp' in body:
                next = self.get_damage_receive_events(report_code, ability_id, body['nextPageTimestamp'], end)
                toappend = next['events']
                body['events'].extend(toappend)
            return body
        else:
            raise Exception

    def get_friend_by_id(self, wc_logs_objects, object_id):
        for wc_logs_object in wc_logs_objects:
            if wc_logs_object['id'] == object_id:
                return wc_logs_object
        return None

    def get_class_color(self, hero_type):
        if hero_type in self.color_classes.keys():
            return self.color_classes[hero_type]
        else:
            return 0