from model.EventLog import EventLog


class HealerSaveService:

    # CONST
    trackedAbilities = [
        # Shaman
        {"name": "ХТТ", "code": 108280},
        {"name": "CЛТ", "code": 98008},
        # Priest
        {"name": "ЕВА", "code": 246287},
        {"name": "Рапча", "code": 47536},
        {"name": "Барьер", "code": 62618},
        {"name": "Спасение", "code": 265202},
        {"name": "ЩитДуши", "code": 109964},
        # MONK
        {"name": "Восстановление сил", "code": 115310},
        # Paladin
        {"name": "Мастер аур", "code": 31821},
        # Druid
        {"name": "Спокойствие", "code": 157982},
        # DPS
        {"name": "Мрак", "code": 196718},
        {"name": "Ободряющий Клич", "code": 97462},
        {"name": "Антимагия", "code": 51052},
        {"name": "Вампирик", "code": 15286},

    ]
    # CONST

    warcraft_logs_service = None
    warcraft_logs_service_v2 = None

    def __init__(self, warcraft_logs_service, warcraft_logs_service_v2):
        self.warcraft_logs_service = warcraft_logs_service
        self.warcraft_logs_service_v2 = warcraft_logs_service_v2

    def create_healer_save_report_v2(self, report_code, fights_report, start, end):
        heroes = fights_report['friendlies']
        context = {
            "report_code": report_code,
            "start": start,
            "end": end,
            "heroes": heroes,
        }

        casts = self.warcraft_logs_service_v2.get_casts_list_v2(context["report_code"],
                                                                self.trackedAbilities,
                                                                context["start"],
                                                                context["end"])
        events = []
        for cast in casts:
            hero = self.get_friend_by_id(context["heroes"], cast['sourceID'])
            color = self.warcraft_logs_service.get_class_color(hero['type'])
            events.append(
                EventLog('healer_save', cast['timestamp'] - context["start"],
                         [["Сдан ", 0], [self.get_ability_name_by_code(self.trackedAbilities, cast['abilityGameID']), color],
                          [" игроком ", 0], [hero['name'], color]], 2))
        return events

    @staticmethod
    def get_ability_name_by_code(tracked_abilities, code):
        for ability in tracked_abilities:
            if ability['code'] == code:
                return ability['name']
        return None

    @staticmethod
    def get_friend_by_id(wc_logs_objects, object_id):
        for wc_logs_object in wc_logs_objects:
            if wc_logs_object['id'] == object_id:
                return wc_logs_object
        return None

