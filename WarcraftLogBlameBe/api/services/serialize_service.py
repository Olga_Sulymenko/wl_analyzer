class SerializationService:
    def __init__(self):
        pass

    @classmethod
    def serialize(cls, data):
        if isinstance(data, list):
            return [cls.serialize(item) for item in data]

        elif isinstance(data, dict):
            data_dict = dict(data)
            # will delete private methods from dict
            for key in data:
                if key.startswith('_'):
                    del data_dict[key]
            return {key: cls.serialize(value) for (key, value) in data_dict.items()}

        try:
            return data.__dict__
        except AttributeError:
            return data
