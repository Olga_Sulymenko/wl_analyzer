from model.EventLog import EventLog


class BossUtilityService:

    warcraft_logs_service = None

    def __init__(self, warcraftlogs_service):
        self.warcraft_logs_service = warcraftlogs_service

    def create_boss_health_report(self, report_code, fights_report, start, end, incheckpoints, boss_guid):
        events = []
        checkpoints = incheckpoints.copy()
        enemy_health = self.warcraft_logs_service.get_boss_health(report_code, start, end)['series']
        boss_health = None
        for boss in enemy_health:
            if boss['guid'] == boss_guid:
                boss_health = boss
                # print(start, end, checkpoints, boss_health)
        if boss_health:
            for health_data in boss_health['data']:
                health_data_timestamp = health_data[0]-start
                if len(checkpoints) > 0:
                    if checkpoints[0] >= health_data[1]:
                        events.append(EventLog('boss_health', health_data_timestamp, [["У босса осталось {}% хп. ".format(
                                    health_data[1]), 4]], 4))
                        checkpoints.remove(checkpoints[0])
            last = boss_health['data'][-1]
            if last[1] == 0:
                word_status = 'Kill'
                index_status = 2
            else:
                word_status = 'Wipe'
                index_status = 6
            events.append(EventLog('boss_health', last[0]-start, [["У босса осталось {0}% {1} хп. ".format(
                            last[1], word_status), index_status]], index_status))
        return events

    def create_aoe_ability_damage_report(self, report_code, start, end, ability_id, ability_name, discreteness=1100,
                                         initial_tick=False):
        report_events = []
        damage_events = self.warcraft_logs_service.get_damage_receive_events(report_code, ability_id, start, end)[
            'events']
        min_damage = 0
        timestamp = 0
        total = 0
        min_percentage = 0
        total_unmitigated = 0
        total_mitigation = 0
        latest_timestamp = 0
        prev_timestamp = 0
        total_percentage = 0
        player_count = 0
        track_initial_damage = initial_tick
        for event in damage_events:
            if 'unmitigatedAmount' in event.keys() and 'amount' in event.keys():
                event_timestamp = event['timestamp']
                damage_taken = event['amount']
                mitigation = event['mitigated'] if 'mitigated' in event else 0
                unmitigated_damage = event['unmitigatedAmount']
                if event['hitType'] == 1:
                    percentage = int((event['hitPoints'] / event['maxHitPoints']) * 100)
                else:
                    percentage = 100
                if track_initial_damage and event_timestamp > timestamp + 500:
                    if min_damage != 0:
                        mitigation_percentage = int((total_mitigation / total_unmitigated) * 100)
                        track_initial_damage = False
                        av_percentage = int(total_percentage / player_count)
                        report_events.append(EventLog('boss_event', prev_timestamp - start, [
                            ["[{0}{1}] {2}к. Initial damage".format(ability_name, "(Initial)", total // 1000), 3],
                            [" [{0}% сейвы]".format(mitigation_percentage), 2],
                            [" [ Просадка: {0}%] ".format(min_percentage), 6 if min_percentage < 30 else 3],
                            [" [ Средняя: {0}%] ".format(av_percentage), 6 if av_percentage < 50 else 3]
                        ]))

                if event_timestamp > timestamp + discreteness:
                    if min_damage != 0:
                        mitigation_percentage = int((total_mitigation / total_unmitigated) * 100)
                        continuous = "Спайк"
                        if prev_timestamp - latest_timestamp > 1400:
                            continuous = "за {0} сек".format(int((prev_timestamp - latest_timestamp)/1000))
                        av_percentage = int(total_percentage / player_count)
                        report_events.append(EventLog('boss_event', prev_timestamp - start, [
                            ["[{0}{1}] {2}к {3}.".format(ability_name, "(Total)", total // 1000, continuous), 3],
                            [" [{0}% сейвы]".format(mitigation_percentage), 2],
                            [" [ Просадка: {0}%] ".format(min_percentage), 6 if min_percentage < 30 else 3],
                            [" [ Средняя: {0}%] ".format(av_percentage), 6 if av_percentage < 50 else 3]
                        ]))
                    timestamp = event_timestamp
                    min_damage = unmitigated_damage
                    min_percentage = percentage
                    total_unmitigated = unmitigated_damage
                    total = damage_taken
                    total_mitigation = mitigation
                    latest_timestamp = 0
                    track_initial_damage = initial_tick
                    total_percentage = percentage
                    player_count = 1
                else:
                    total += damage_taken
                    total_unmitigated += unmitigated_damage
                    total_mitigation += mitigation
                    if min_damage > unmitigated_damage or min_damage == 0:
                        min_damage = unmitigated_damage
                    if min_percentage > percentage or min_percentage == 0:
                        min_percentage = percentage
                    if latest_timestamp == 0:
                        latest_timestamp = event_timestamp
                    prev_timestamp = event_timestamp
                    total_percentage += percentage
                    player_count += 1
        if min_damage != 0:
            mitigation_percentage = int(total_mitigation / total_unmitigated)
            continuous = "Спайк"
            if prev_timestamp - latest_timestamp > 1400:
                continuous = "за {0} сек".format(int((prev_timestamp - latest_timestamp) / 1000))
            av_percentage = int(total_percentage / player_count)
            report_events.append(EventLog('boss_event', prev_timestamp - start, [
                ["[{0}] {1}к {2}.".format(ability_name, total // 1000, continuous), 3],
                [" [{0}% сейвы]".format(mitigation_percentage), 2],
                [" [ Просадка: {0}%] ".format(min_percentage), 6 if min_percentage < 30 else 3],
                [" [ Средняя: {0}%] ".format(av_percentage), 6 if av_percentage < 50 else 3]
            ]))
        return report_events
