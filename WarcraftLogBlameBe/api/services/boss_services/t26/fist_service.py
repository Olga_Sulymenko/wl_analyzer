from model.EventLog import EventLog


class FistService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service
    total_health = 33260000
    @staticmethod
    def get_encounter_id():
        return 2399

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        events = []
        boss_stun_buff_id = 331314
        enemy_buffs_list = self.warcraft_logs_service.get_buffs_list_on_bosses(report_code, boss_stun_buff_id,
                                                                               start, end)['events']

        expected = [
            14, 12, 9, 11, 9, 7, 9, 16, 10, 6
        ]
        prev_timestamp = start
        total_damage = 0
        phase = 0
        for event in enemy_buffs_list:
            timestamp = event['timestamp']
            damage_table = self.warcraft_logs_service.get_players_damage_in_time_window(report_code,
                                                                                        prev_timestamp,
                                                                                        timestamp
                                                                                        )['entries']
            phase_damage = damage_table[0]['total'] if len(damage_table) > 0 else 0
            total_damage += phase_damage
            prev_timestamp = timestamp
            if event['type'] == 'applybuff':
                events.append(self._phase_event('Удар в колону {0}'.format(phase), self._left_health(total_damage), timestamp-start, phase_damage, expected[phase*2]))
            if event['type'] == 'removebuff':
                events.append(self._phase_event('Босс пришел в себя {0}'.format(phase), self._left_health(total_damage), timestamp-start, phase_damage, expected[phase*2+1]))

                phase += 1
        damage_table = self.warcraft_logs_service.get_players_damage_in_time_window(report_code,
                                                                                    prev_timestamp,
                                                                                    end
                                                                                    )['entries']
        phase_damage = damage_table[0]['total'] if len(damage_table) > 0 else 0
        total_damage += phase_damage
        events.append(self._phase_event('Конец боя', self._left_health(total_damage), end-start, phase_damage, 0))

        return events

    def _left_health(self, total_damage):
        left = (self.total_health - total_damage)/self.total_health
        return int(left*100)

    def _phase_event(self, title, left, timestamp, phase_damage, expected):
        damage, damage_color = self._format_damage(phase_damage, expected)
        return EventLog('boss_event', timestamp,
                        [['{1}% хп. {0}. '.format(title, left, ), 3],
                         ["Урона за фазу: {}".format(damage), damage_color]], 3)

    def create_detailed_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        events = []

        return events

    def _format_damage(self, damage, expected):
        percentage = int(damage/self.total_health*1000)/10
        expected_str = "-0.0%"
        color = 1
        if expected > percentage:
            color = 1
            expected_str = "-{}%".format(max(int((expected-percentage)*10)/10, 0.1))
        elif percentage > expected:
            color = 5
            expected_str = "+{}%".format(int((percentage-expected)*10)/10)

        damage_str = "{}м ({}%) {}".format(int(damage / 1000)/1000, percentage, expected_str)
        return damage_str, color
