from model.EventLog import EventLog


class ShriekwingService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service

    @staticmethod
    def get_encounter_id():
        return 2398

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        boss_guid = 164406
        # ReverberatingScreamID = 344112
        # ReverberatingScreamDotID = 344114
        WaveOfBloodID = 345397
        events = []
        checkpoints = [80, 60, 40, 20, 15, 10, 5]

        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)

        # events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end,
        #                                                                          ReverberatingScreamID,
        #                                                                          "ReverberatingScream",
        #                                                                          discreteness=1400))
        # events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end,
        #                                                                          ReverberatingScreamDotID,
        #                                                                          "ReverberatingScream(Dot)",
        #                                                                          discreteness=16000))
        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end,
                                                                                 WaveOfBloodID,
                                                                                 "WaveOfBlood",
                                                                                 discreteness=14000, initial_tick=True))

        events.extend(self._generate_phases_timestamp(start, end))

        events.extend(health_events)
        return events

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass

    @staticmethod
    def _generate_phases_timestamp(start, end):
        result = []
        timestamp = 0
        phaseCount = 0
        while timestamp < (end - start)/1000:
            phaseCount += 1
            timestamp += 100
            if timestamp < (end - start)/1000:
                result.append(EventLog('boss_event', timestamp*1000,
                                       [['Фаза пряток {0}.'.format(phaseCount), 3]], 3))
            timestamp += 50
            if timestamp < (end - start)/1000:
                result.append(EventLog('boss_event', timestamp*1000,
                                       [['Фаза сражения {0}.'.format(phaseCount+1), 3]], 3))

        return result
