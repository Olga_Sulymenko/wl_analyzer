from model.EventLog import EventLog


class DestroyerService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service

    @staticmethod
    def get_encounter_id():
        return 2383

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        boss_guid = 164261
        KillAllSpellID = 329455
        KillAllSpellID2 = 329742
        KillAllSpellID3 = 334523
        LazersID = 334228
        events = []
        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end,
                                                                                 KillAllSpellID,
                                                                                 "Опустошение"))
        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end,
                                                                                 KillAllSpellID2,
                                                                                 "Истребление"))
        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end,
                                                                                 KillAllSpellID3,
                                                                                 "Полгощение",
                                                                                 discreteness=15000))
        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end,
                                                                                 LazersID,
                                                                                 "Лазеры"))
        events.extend(self._generate_phases_timestamp(start, end))
        events.extend(self._generate_phases_timestamp_2(start, end))
        return events

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass

    @staticmethod
    def _generate_phases_timestamp(start, end):
        result = []
        timestamp = -3
        phaseCount = 0
        while timestamp < (end - start)/1000:
            phaseCount += 1
            timestamp += 96
            if timestamp < (end - start)/1000:
                result.append(EventLog('boss_event', timestamp*1000,
                                       [['Притяжка {0}.'.format(phaseCount), 3]], 3))

        return result

    @staticmethod
    def _generate_phases_timestamp_2(start, end):
        result = []
        timestamp = -21
        phaseCount = 0
        while timestamp < (end - start)/1000:
            phaseCount += 1
            timestamp += 24
            if phaseCount == 5:
                phaseCount = 1
            if timestamp < (end - start)/1000:
                result.append(EventLog('boss_event', timestamp*1000,
                                       [['Миазмы {0}.'.format(phaseCount), 3]], 3))

        return result
