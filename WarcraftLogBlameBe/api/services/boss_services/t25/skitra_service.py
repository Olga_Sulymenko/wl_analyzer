from model.EventLog import EventLog


class SkitraService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service

    @staticmethod
    def get_encounter_id():
        return 2334

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        events = []
        checkpoints = [15, 10, 5, 2, 1]
        boss_guid = 157238
        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)

        enemy_buffs_list = self.warcraft_logs_service.get_buffs_list_on_bosses(report_code, 307725, start, end)['events']

        phase_counter = 0
        prev = 0
        debuffs_estimate = 0
        for buff in enemy_buffs_list:
            if buff['type'] == 'applybuff':
                phase_counter += 1
                timestamp = buff['timestamp'] - start
                prev = timestamp
                events.append(EventLog('boss_event', timestamp, [['Фаза илюзий {0}.'.format(phase_counter), 3]], 3))
            if buff['type'] == 'removebuff':
                timestamp = buff['timestamp'] - start
                time_on_phase = int((timestamp - prev)/1000)
                debuffs_estimate += (time_on_phase -2) // 5
                events.append(EventLog('boss_event', timestamp, [['Фаза босса {0}. Переходка длилась {1} сек. Сложность ~{2}'.format(phase_counter+1, time_on_phase, debuffs_estimate), 3]], 3))

        events.extend(health_events)
        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end, 309687, "Вспышка больного рассудка"))
        return events

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass
