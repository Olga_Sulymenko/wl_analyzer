from model.EventLog import EventLog


class WrathionService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service


    @staticmethod
    def get_encounter_id():
        return 2329

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):

        events = []
        heroes = fights_report['friendlies']
        difficulty = current_fight['difficulty']
        if difficulty == 5:
            scale_debuf = self.warcraft_logs_service.get_debufs_events_on_heroes(report_code, 309733, start, end)['events']  # Mythic
        else:
            scale_debuf = self.warcraft_logs_service.get_debufs_events_on_heroes(report_code, 307013, start, end)['events'] # Heroic

        scales_per_player = {}
        for event in scale_debuf:
            if event['targetID'] in scales_per_player:
                scales_per_player[event['targetID']].append(event)
            else:
                scales_per_player[event['targetID']] = [event]

        for player in scales_per_player.keys():
            hero = self.get_friend_by_id(heroes, player)
            color = self.warcraft_logs_service.get_class_color(hero['type'])
            stack = 0
            apply_time = 0
            for event in scales_per_player[player] :
                if event['type'] == 'applydebuff':
                    apply_time = event['timestamp']
                    stack = 0
                elif event['type'] == 'removedebuffstack':
                    stack += 1
                elif event['type'] == 'removedebuff':
                    if (event['timestamp'] - apply_time) < 10000 + (stack*1000):
                        stack += 1
                        events.append(EventLog('boss_event', event['timestamp'] - start, [[' Игрок ', 0], [hero['name'], color], [' разрушил {} осколков '.format(stack), 0]], 0))
                    else:
                        events.append(EventLog('boss_event', event['timestamp'] - start,
                                               [[' Игрок ', 0], [hero['name'], color], [' МЕДЛИЛ и разрушил {} осколков '.format(stack), 0]], 5))
        free_events = []
        spell_id = 116841
        casts_done = self.warcraft_logs_service.get_casts_list(report_code, spell_id, start,
                                                               end)['events']
        for event in casts_done:
            ability_name = event['ability']['name']
            caster = self.get_friend_by_id(heroes, event['sourceID'])
            caster_color = self.warcraft_logs_service.get_class_color(caster['type'])
            target = self.get_friend_by_id(heroes, event['targetID'])
            target_color = self.warcraft_logs_service.get_class_color(target['type'])
            free_events.append(EventLog('cast_event', event['timestamp'] - start,
                                   [[' Игрок ', 0], [caster['name'], caster_color], [' использовал способность ', 0],
                                    [ability_name, caster_color], [' на ', 0],
                                    [target['name'], target_color]], 0))

        spell_id = 1044
        casts_done = self.warcraft_logs_service.get_casts_list(report_code, spell_id, start,
                                                               end)['events']
        for event in casts_done:
            ability_name = event['ability']['name']
            caster = self.get_friend_by_id(heroes, event['sourceID'])
            caster_color = self.warcraft_logs_service.get_class_color(caster['type'])
            target = self.get_friend_by_id(heroes, event['targetID'])
            target_color = self.warcraft_logs_service.get_class_color(target['type'])
            free_events.append(EventLog('cast_event', event['timestamp'] - start,
                                   [[' Игрок ', 0], [caster['name'], caster_color], [' использовал способность ', 0],
                                    [ability_name, caster_color], [' на ', 0],
                                    [target['name'], target_color]], 0))

        events.extend(free_events)

        boss_guid = 156818
        checkpoints = [80, 60, 40, 20, 15, 10, 5]

        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)
        events.extend(health_events)
        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end, 306751, "Пылающий катаклизм", discreteness=8400))
        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end, 306247, "Испепеление", discreteness=1400))
        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end, 306289, "Ураганный удар", discreteness=1400))
        return events




    @staticmethod
    def get_friend_by_id(wc_logs_objects, object_id):
        for wc_logs_object in wc_logs_objects:
            if wc_logs_object['id'] == object_id:
                return wc_logs_object
        return None

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass