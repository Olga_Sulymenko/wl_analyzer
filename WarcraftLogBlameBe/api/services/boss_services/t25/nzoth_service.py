from model.EventLog import EventLog


class NzothService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service

    @staticmethod
    def get_encounter_id():
        return 2344

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        enemy_buffs_list = self.warcraft_logs_service.get_buffs_list_on_bosses(report_code, 319309, start, end)['events']
        events = []
        prev = 0
        for buff in enemy_buffs_list:
            if buff['type'] == 'applybuff':
                timestamp = buff['timestamp'] - start
                prev = timestamp
                events.append(EventLog('boss_event', timestamp, [['Дебаф повесился на босса.', 3]], 3))
            if buff['type'] == 'applybuffstack':
                timestamp = buff['timestamp'] - start
                events.append(EventLog('boss_event', timestamp, [['Дебаф {} повесился на босса. Через {}.{}сек после предыдущего'.format(buff['stack'],
                                                                                                                                        int((timestamp - prev) / 1000),
                                                                                                                                        int(((timestamp - prev) % 1000)/100)), 3]], 3))
                prev = timestamp
        return events


    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass
