from model.EventLog import EventLog


class DestagathService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.boss_utility_service = boss_utility_service
        self.warcraft_logs_service = warcraft_logs_service


    @staticmethod
    def get_encounter_id():
        return 2343

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        boss_guid = 157602
        events = []
        checkpoints = [80, 60, 40, 20, 15, 10, 5]
        heroes = fights_report['friendlies']

        ability_id = 308377
        boss_damage_debufs =\
            self.warcraft_logs_service.get_debufs_events_on_heroes(report_code, ability_id, start, end)['events']
        boss_damage_debuf_filtered = []
        for event in boss_damage_debufs:
            if event['type'] == "applydebuff":
                boss_damage_debuf_filtered.append(event)

        debuff_duration = 30000
        for event in boss_damage_debuf_filtered:
            hero = self.get_friend_by_id(heroes, event['targetID'])
            damage_table = self.warcraft_logs_service.get_player_damage_in_time_window(report_code, event['targetID'], event['timestamp'], event['timestamp']+debuff_duration)['entries']
            events.append(self._generate_destagat_debuff_damage_event(hero, event['timestamp']-start, damage_table))

        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)
        events.extend(health_events)
        return events

    def _generate_destagat_debuff_damage_event(self, player, timestamp, damage_table):
        boss_damage = 0
        non_boss_damage = 0
        for row in damage_table:
            if row['type'] == 'Boss':
                boss_damage += row['total']
            else:
                non_boss_damage += row['total']
        color = self.warcraft_logs_service.get_class_color(player['type'])
        hero_name = player['name']
        return EventLog('boss_event', timestamp, [["Игрок ", 0],
                                                  [hero_name, color],
                                                  [" взял сферку, и нанес ", 0],
                                                  [boss_damage //1000, 1],
                                                  ["к урона по боссу. + ", 0],
                                                  [non_boss_damage //1000, 1],
                                                  ["к по адам", 0]], 4)

    @staticmethod
    def get_friend_by_id(wc_logs_objects, object_id):
        for wc_logs_object in wc_logs_objects:
            if wc_logs_object['id'] == object_id:
                return wc_logs_object
        return None

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass