from model.EventLog import EventLog


class MautService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service

    @staticmethod
    def get_encounter_id():
        return 2327

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        events =[]
        boss_guid = 156523
        checkpoints = [80, 60, 40, 30, 20, 15, 10, 5]
        heroes = fights_report['friendlies']
        pets = fights_report['friendlyPets']
        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)
        events = []
        events.extend(health_events)

        enemy_mana = self.warcraft_logs_service.get_boss_mana(report_code, start, end)['series']
        boss_mana = None
        monument_phase_time = []
        for boss in enemy_mana:
            if boss['guid'] == boss_guid:
                boss_mana = boss
        if boss_mana:
            phases_count = 1
            latest_phase = 1
            for mana_data in boss_mana['data']:
                mana_data_timestamp = mana_data[0] - start
                if mana_data[1] == 100 and latest_phase == 1:
                    monument_phase_time.append([mana_data[0]])
                    events.append(
                            EventLog('boss_mana', mana_data_timestamp, [["Фаза статуи {0}. ".format(phases_count), 4]], 4))
                    latest_phase = 2
                if mana_data[1] == 0 and latest_phase == 2:
                    phases_count += 1
                    last = len(monument_phase_time)-1
                    monument_phase_time[last].append(mana_data[0])
                    events.append(
                        EventLog('boss_mana', mana_data_timestamp, [["Обычная фаза {0}. ".format(phases_count), 4]],
                                 4))
                    latest_phase = 1

        all_phases_times = []
        phases_with_curse = []
        if monument_phase_time:
            for index, phase in enumerate(monument_phase_time):
                if index == 0:
                    phases_with_curse.append([start, phase[0]])
                    all_phases_times.append([start, phase[0]])
                    all_phases_times.append(phase)
                elif index == len(monument_phase_time)-1 :
                    phases_with_curse.append([phases_with_curse[-1][1], phase[0]])
                    phases_with_curse.append([phase[1], end])
                    all_phases_times.append([all_phases_times[-1][1], phase[0]])
                    all_phases_times.append(phase)
                    all_phases_times.append([phase[1], end])
                else:
                    phases_with_curse.append([all_phases_times[-1][1], phase[0]])
                    all_phases_times.append([all_phases_times[-1][1], phase[0]])
                    all_phases_times.append(phase)

        for phase in all_phases_times:
            message = [['Развеивание магии за фазу ', 0]]
            message.extend(self.get_dispell(report_code, heroes, pets, phase[0], phase[1]))
            #         (4987   paladin)  (77130 shaman)   (527 priest)    (115450 monk )   (88423  druid)
            if message != [['Развеивание магии за фазу ', 0]]:
                events.append(EventLog('decurse_event', phase[1] - start -1, message, 2))

        enemy_casts_list = self.warcraft_logs_service.get_enemy_casts_list_events(report_code, 314337, start, end)[
            'events']

        curse_counter = 0
        for cast in enemy_casts_list:
            if cast['type'] == 'cast':
                curse_counter += 1
                timestamp = cast['timestamp'] - start
                events.append(EventLog('boss_event', timestamp, [['Древнее проклятие {0}.'.format(curse_counter), 3]], 3))

        dark_manifestation_curse_counter = 0
        dark_manifestation_casts_list = self.warcraft_logs_service.get_enemy_casts_list_events(report_code, 308903, start, end)[
            'events']

        for cast in dark_manifestation_casts_list:
            if cast['type'] == 'cast':
                dark_manifestation_curse_counter += 1
                timestamp = cast['timestamp'] - start
                events.append(EventLog('boss_event', timestamp, [['Призыв ада {0}.'.format(dark_manifestation_curse_counter), 3]], 3))

        events.extend(self.boss_utility_service.create_aoe_ability_damage_report(report_code, start, end, 308168, "Поглощающие тени", discreteness=4000))
        return events

    def get_dispell(self, report_code, heroes, pets,  start, end):
        casts_done = self.warcraft_logs_service.get_dispell_list(report_code, start, end)['events']
        message = []
        decursers = {}
        for cast in casts_done:
            if cast['sourceID'] in decursers.keys():
                decursers[cast['sourceID']] += 1
            else:
                decursers.update({cast['sourceID'] : 1})
        for key, item in decursers.items():
            hero = self.get_friend_by_id(heroes, key)
            if hero is None:
                pet = self.get_friend_by_id(pets, key)
                if pet is not None:
                    hero = self.get_friend_by_id(heroes, pet['petOwner'])
            if hero:
                color = self.warcraft_logs_service.get_class_color(hero['type'])
                message.extend([[hero['name'], color], [' -- ', 0], [item, 0]])
        return message

    def get_decurse(self, report_code, heroes, start, end, spell_id):
        casts_done = self.warcraft_logs_service.get_casts_list(report_code, spell_id, start,
                                                               end)['events']
        decursers = {}
        message = []
        for cast in casts_done:
            if cast['sourceID'] in decursers.keys():
                decursers[cast['sourceID']] += 1
            else:
                decursers.update({cast['sourceID']: 1})
        for key, item in decursers.items():
            hero = self.get_friend_by_id(heroes, key)
            color = self.warcraft_logs_service.get_class_color(hero['type'])
            message.extend([[hero['name'], color], [item, 0]])
        return message



    @staticmethod
    def get_friend_by_id(wc_logs_objects, object_id):
        for wc_logs_object in wc_logs_objects:
            if wc_logs_object['id'] == object_id:
                return wc_logs_object
        return None


    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass
