from model.EventLog import EventLog


class ShadharService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service

    @staticmethod
    def get_encounter_id():
        return 2335

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        boss_guid = 157231
        events = []
        checkpoints = [80, 60, 40, 20, 15, 10, 5]

        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)
        events.extend(health_events)
        return events

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass
