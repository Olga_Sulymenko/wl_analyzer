from model.EventLog import EventLog


class VexionaService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service

    @staticmethod
    def get_encounter_id():
        return 2336

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        events = []
        heroes = fights_report['friendlies']
        checkpoints = [80, 60, 40, 30, 20, 15, 10, 5]
        boss_guid = 157354
        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)
        events.extend(health_events)

        ability_id = 307371
        damage_events = self.warcraft_logs_service.get_damage_receive_events(report_code, ability_id, start, end)[
            'events']
        min_damage = 0
        timestamp = 0
        tank_events = []
        for event in damage_events:
            if 'unmitigatedAmount' in event.keys():
                event_timestamp = event['timestamp']
                event_damage = event['unmitigatedAmount']
                if event_timestamp > timestamp + 1000:
                    if min_damage != 0:
                        tank_events.append(EventLog('boss_event', timestamp - start, [["Урон от танковского дебафа по рейду {0}к. ".format(min_damage // 1000), 0]],
                                               0))
                    timestamp = event_timestamp
                    min_damage = event_damage
                else:
                    if min_damage > event_damage or min_damage == 0:
                        min_damage = event_damage

        if min_damage != 0:
            tank_events.append(EventLog('boss_event', timestamp - start,
                                        [["Урон от танковского дебафа по рейду {}к. ".format(min_damage // 1000), 0]],
                                        0))
        events.extend(tank_events)
        return events

        pass

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass