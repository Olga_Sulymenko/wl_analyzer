from model.Guild import Guild
from services.warcraftlogs_service import WarCraftLogsService
from services.boss_utility_service import BossUtilityService
from services.warcraftlogs_service_v2 import WarCraftLogsServiceV2
from services.boss_service import BossService
from services.guild_services.guild_service import GuildService
from services.notes_services.notes_service import NotesService

from flask import jsonify, g, request

from services.healer_save_service import HealerSaveService
from services.death_service import DeathService
from services.report_util_service import ReportUtilService
from services.serialize_service import SerializationService

import time


class Endpoints:

    def __init__(self, app):
        self.warcraft_logs_service_v2 = WarCraftLogsServiceV2(app.config['WL_API_V2_TOKEN'])
        self.warcraft_logs_service = WarCraftLogsService(app.config['WL_API_V1_TOKEN'])
        self.healer_save_service = HealerSaveService(self.warcraft_logs_service, self.warcraft_logs_service_v2)
        self.death_service = DeathService(self.warcraft_logs_service)
        self.report_util_service = ReportUtilService()
        self.serialize_service = SerializationService()
        self.boss_utility_service = BossUtilityService(self.warcraft_logs_service)
        self.boss_service = BossService(self.warcraft_logs_service, self.boss_utility_service)
        self.guild_service = GuildService()
        self.notes_service = NotesService()

    def setup_routes(self, app, cache):
        @app.route('/')
        def root():
            return "Response"

        @app.route('/ready')
        def ready():
            return "ready"

        @app.route('/notes/t26/8')
        def get_note_debug():
            note = self.notes_service.getNote()
            return note

        @app.route('/notes/t26/3')
        def get_note_t26_3():
            note = self.notes_service.getT26B3Note()
            return note

        @app.route('/notes/t26/5')
        def get_note_t26_5():
            note = self.notes_service.getT26B5Note()
            return note

        @app.route('/guild/init')
        def post_guild():
            guild = Guild("eu", "гордунни", "денди")
            persisted_guild = self.guild_service.save_guild_definition(guild)
            return jsonify(persisted_guild.json())

        @app.route('/guild/<region>/<server>/<guild_name>')
        def get_guild(region, server, guild_name):
            guild = self.guild_service.get_guild_definition(region, server, guild_name)
            return jsonify(guild)

        @app.route('/report/<report_code>')
        def report_by_code(report_code):
            fights_report = self.warcraft_logs_service.get_report_fights(report_code)
            model = {'report': fights_report,
                     'report_code': report_code}
            return jsonify(model)

        @app.route('/report/<report_code>/fight/<int:fight_id>/healer_saves')
        @cache.cached()
        def heal_saves(report_code, fight_id):
            # TODO MUST be taken from cache
            report_meta_data = self.warcraft_logs_service.get_report_fights(report_code)
            current_fight = self.report_util_service.get_fight_by_id(report_meta_data, fight_id)
            start = current_fight['start_time']
            end = current_fight['end_time']
            report_events = self.healer_save_service.create_healer_save_report_v2(report_code, report_meta_data, start, end)

            model = {'events': report_events,
                     'fight_id': fight_id,
                     'report_code': report_code}
            return jsonify(self.serialize_service.serialize(model))

        @app.route('/report/<report_code>/fight/<int:fight_id>/playres_death')
        @cache.cached()
        def players_death(report_code, fight_id):
            # TODO MUST be taken from cache
            report_meta_data = self.warcraft_logs_service.get_report_fights(report_code)
            current_fight = self.report_util_service.get_fight_by_id(report_meta_data, fight_id)
            start = current_fight['start_time']
            end = current_fight['end_time']
            report_events = self.death_service.create_avoidable_death_report(report_code, report_meta_data, start, end)
            model = {'events': report_events,
                     'fight_id': fight_id,
                     'report_code': report_code}
            return jsonify(self.serialize_service.serialize(model))

        @app.route('/report/<report_code>/fight/<int:fight_id>/boss_event')
        @cache.cached()
        def boss_event(report_code, fight_id):
            # TODO MUST be taken from cache
            report_meta_data = self.warcraft_logs_service.get_report_fights(report_code)
            current_fight = self.report_util_service.get_fight_by_id(report_meta_data, fight_id)
            start = current_fight['start_time']
            end = current_fight['end_time']
            report_events = self.boss_service.create_fast_boss_events_report(report_code, current_fight, report_meta_data, start, end)
            model = {'events': report_events,
                     'fight_id': fight_id,
                     'report_code': report_code}
            return jsonify(self.serialize_service.serialize(model))

        @app.route('/report/<report_code>/fight/<int:fight_id>/detailed')
        @cache.cached()
        def boss_event_detailed(report_code, fight_id):
            # TODO MUST be taken from cache
            report_meta_data = self.warcraft_logs_service.get_report_fights(report_code)
            current_fight = self.report_util_service.get_fight_by_id(report_meta_data, fight_id)
            start = current_fight['start_time']
            end = current_fight['end_time']
            report = self.boss_service.create_detailed_boss_events_report(report_code, current_fight,
                                                                          report_meta_data, start, end)
            return jsonify(report)

        @app.before_request
        def start_timer():
            g.start = time.time()

        @app.after_request
        def log_request(response):
            now = time.time()
            duration = round(now - g.start, 6)  # to the microsecond
            ip_address = request.headers.get("X-Forwarded-For", request.remote_addr)
            host = request.host.split(":", 1)[0]

            app.logger.info("{method} {host}{path} : {status} from {ip} took {duration}s."
                            .format(method=request.method, path=request.path, status=response.status_code,
                                    duration=duration, ip=ip_address, host=host))

            return response
