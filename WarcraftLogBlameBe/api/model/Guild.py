import time


class Guild:

    def __init__(self, region, server, name):
        self.region = region
        self.server = server
        self.name = name
        self.last_modified = time.time()
        self.created = time.time()

    @classmethod
    def from_definition(cls, guild_definition):
        guild = Guild(guild_definition['region'], guild_definition['server'], guild_definition['name'])
        guild.last_modified = guild_definition['last_modified']
        guild.created = guild_definition['created']
        return guild

    def json(self):
        return {
            "region": self.region,
            "server": self.server,
            "name": self.name,
            "last_modified": self.last_modified,
            "created": self.created,
        }
